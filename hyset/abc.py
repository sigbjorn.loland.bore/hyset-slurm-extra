from abc import ABC, abstractmethod


class ComputeSettings(ABC):
    """Base class for Architectures."""

    @property
    @abstractmethod
    def name(self) -> str:
        """Print architecture type."""
        pass  # pragma no cover
