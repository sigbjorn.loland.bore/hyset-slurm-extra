from typing import Optional

from .abc import ComputeSettings
from .local import LocalArch
from .saga import SagaArch

ARCHS = {
    'local': LocalArch,
    'saga': SagaArch,
}


def create_compute_settings(target_arch: Optional[str] = None, **kwargs
                            ) -> ComputeSettings:
    """Create computational environment.

    Parameter
    ---------
    target_arch : str, optional
        architecture on which Method should run, defaults to 'local'


    """
    arch: ComputeSettings
    target_arch = 'local' if target_arch is None else target_arch
    try:
        arch = ARCHS[target_arch.lower()](**kwargs)
    except (KeyError, NotImplementedError):
        raise NotImplementedError('could find create target architecture' +
                                  f' {target_arch}')
    return arch
