# from __future__ import annotations

import os
from pathlib import Path
from typing import List, Optional, Union

from ..abc import ComputeSettings
from .runner import LocalRunner

PathLike = Union[str, Path]
PathLikeList = Union[PathLike, List[PathLike]]

# unified creation of computational environment/architecture
# every interface has translation routines (if necessary) to convert,
# e.g. for mpi
#  hygo has an own translation dp -> hsp


class LocalArch(ComputeSettings):
    """Local computational environment."""

    def __init__(self,
                 work_dir: Optional[PathLike] = None,
                 scratch_dir: Optional[PathLike] = None,
                 path: Optional[PathLikeList] = None,
                 lib: Optional[PathLikeList] = None,
                 env_vars: Optional[dict] = None,
                 mpi_procs: Optional[int] = None,
                 smp_threads: Optional[int] = None,
                 force_recompute: Optional[bool] = None,
                 memory: Optional[int] = None,
                 launcher: Optional[Union[str, list]] = None,
                 options: Optional[dict] = None,
                 debug: Optional[bool] = False) -> None:
        """Set Local ComputeSettings.

        Parameter
        ---------
        work_dir : Union[str, Path], optional
            path to work directory, defaults to cwd
        scratch_dir : Union[str, Path], optional
            path to scratch directory, defaults to cwd
        path: Union[str, Union[str, Path]], optional
            path to be added to environment variable $PATH
        lib: Union[str, Union[str, Path]], optional
            path to be added to environment variable $LD_LIBRARY_PATH
        env: dict, optional
            environmental variables to be set
        mpi_procs: int, optional
            number of mpi communicators
        smp_threads: int, optional
            value of OMP_NUM_THREADS to be set
        memory: int, optional
            memory in butes
        launcher: str or list(str), optional
            launcher for program, e.g. 'mpirun'
        options: dict, optional
            further options, e.g. 'parser', 'asynchrounous', 'force_recompute'
        debug: bool
            debug printing options

        """
        self.arch_type = 'local'
        self.options = {} if not options else options

        self.asynchronous = self.options.get('asynchronous', False)
        self.force_recompute = self.options.get('force_recompute', False)
        if force_recompute is not None:
            self.force_recompute = force_recompute
        self.print_level = self.options.get('print_level', 1)
        self.mpi_procs = mpi_procs
        self.smp_threads = smp_threads
        self.memory = memory
        self.debug = debug

        self.launcher: List[str]
        if launcher is not None:
            if isinstance(launcher, str):
                self.launcher = launcher.split()
            elif isinstance(launcher, list):
                self.launcher = launcher
        else:
            self.launcher = []

        self.work_dir = LocalArch._set_dir(work_dir)
        self.scratch_dir = LocalArch._set_dir(scratch_dir)

        self.env = os.environ.copy()

        if path is not None:
            path = path if isinstance(path, list) else [path]
            for subpath in path:
                self.env['PATH'] = f'{subpath}:' + self.env['PATH']

        if lib is not None:
            lib = lib if isinstance(lib, list) else [lib]
            for sublib in lib:
                ld_str = f'{sublib}:' + self.env['LD_LIBRARY_PATH']
                self.env['LD_LIBRARY_PATH'] = ld_str

        if env_vars is not None:
            self.env.update(env_vars)

        if self.smp_threads is not None:
            self.env.update({'OMP_NUM_THREADS': str(self.smp_threads)})

        self.Runner = LocalRunner(self)

    def __repr__(self):
        """Return a printable representation of Environment instance."""
        keywords = [f'{key}={value!r}' for key, value in self.__dict__.items()]
        return '{}({})'.format(type(self).__name__, ', '.join(keywords))

    @property
    def name(self) -> str:
        """Print environment type."""
        return 'local'

    # @classmethod
    # def add_var_to_env(cls, env: dict, vars: dict) -> dict:

    # @classmethod
    # def add_to_env_var(cls, env: Union[str, PathLike, dict], name: str,
    #                    value: PathLike) -> dict:

    # @classmethod
    # def add_vars_to_env(cls, vars: dict, add_vars: bool) -> dict:
    #     """Add variables to environment."""
    #     env: dict
    #     env = os.environ.copy() if add_vars else {}
    #     env.update(vars)
    #     return env

    # @classmethod
    # def add_path_to_env_var(cls, var_str: str, input_dirs: PathLikeList,
    #                         add_vars: bool) -> Union[dict, str]:
    #     """Add path to PATH."""
    #     if not isinstance(input_dirs, (list, tuple)):
    #        input_dirs = list(input_dirs
    #     env: dict
    #     env = os.environ.copy() if add_vars else {}
    #     for input_dir in input_dirs:
    #         try:
    #             path = Path(input_dir)
    #         except Exception:
    #             raise Exception(f'could not create path {input_dir}')

    #         if not path.exists():
    #             raise Exception(f'path {path} does not exist')

    #         env[var_str] = f'{path}:' + env[var_str]

    #     return env[var_str]

    def find_bin_in_env(self, program: str) -> str:
        """Find binary in PATH environment.

        Parameter
        ---------
        program : str
            name of binary

        Returns
        -------
        str
            if found, absolute path of binary, else program

        """
        paths = self.env['PATH'].split(':')
        paths.append(str(Path.cwd()))
        for folder in paths:
            try:
                folder_path = Path(folder)
            except (OSError, TypeError):
                continue
            else:
                if not folder_path.exists():
                    continue
            for f in folder_path.iterdir():
                if f.name == program:
                    return str(f)

        return program

    @classmethod
    def _set_dir(cls, input_dir: Union[PathLike, None]) -> Path:
        """Set directory.

        Parameter
        ---------
        input_dir: :obj:`pathlib.Path` or str
            path to input dir. if input_dir is None,
            cwd() is used

        Returns
        -------
        Path
            if input_dir is None, returns cwd, else input_dir
            is created.

        """
        output_dir: Path
        if input_dir is None:
            output_dir = Path.cwd()
        else:
            output_dir = LocalArch.create_path(
                                Path(input_dir), create=True)  # type: ignore
        return output_dir

    @classmethod
    def create_path(cls,
                    input_dirs: PathLikeList,
                    prefix: Optional[PathLike] = None,
                    create: Optional[bool] = False) -> Union[Path, List[Path]]:
        """Set and check path.

        Parameter
        ---------
        input_dirs : (list of) :obj:`pathlib.Path`
            paths to create/check existance
        prefix: :obj:`pathlib.Path`, optional
            prefix for input_dirs, defaults to None
        create: bool
            create folder if it does not exists

        Returns
        -------
        :obj:`pathlib.Path` or list(:obj:`pathlib.Path`)
            created paths

        """
        if not isinstance(input_dirs, (list, tuple)):
            input_dirs = [input_dirs]
        paths: List[Path] = []
        for path in input_dirs:
            try:
                if prefix is not None:
                    prefix = Path(prefix)
                    path = path = Path(prefix / path)
                else:
                    path = Path(path)
            except Exception:
                raise Exception(f'could not create path from path {path}')
            else:
                paths.append(path)
            if create:
                path.mkdir(mode=0o777, parents=True, exist_ok=True)

            if not path.exists():
                raise Exception(f'path {path} does not exist')

        return paths if len(paths) > 1 else paths[0]

    @classmethod
    def delete_path(cls, input_dirs: PathLikeList,
                    recursive: Optional[bool] = True) -> None:
        """Delete existing path.

        Parameter
        ---------
        input_dirs : (list of) :obj:`pathlib.Path`
            files/directories to remove
        recursive: bool, optional
            remove recursively, defaults to True

        """
        if not isinstance(input_dirs, (list, tuple)):
            input_dirs = [input_dirs]
        for input_dir in input_dirs:
            try:
                path = Path(input_dir)
            except Exception:
                raise Exception(f'could not create path {type(input_dir)}')

            if not path.exists():
                raise Exception(f'path {path} does not exist')

            try:
                if any([path.is_file(), path.is_symlink()]):
                    path.unlink()
                elif path.is_dir():
                    if recursive:
                        for child in path.iterdir():
                            child.unlink()
                    path.rmdir()
            except Exception:
                raise Exception(f'could not remove path {path}')

        if path.exists():
            raise Exception(f'could not remove path {path}')

    def clean_dir(self, path: Path, exceptions: Union[str, List[str]]) -> None:
        """Clean directory.

        Parameter
        ---------
        path : :obj:`pathlib.Path`
            folder to remove
        exceptions : List[str] or str
            list of exeptions, e.g. [ 'myfile', '*.inp' ]

        """
        if not path.exists():
            return
        if isinstance(exceptions, str):
            exceptions = [str(exceptions)]
        files = list(path.iterdir())
        for file in files:
            keep = LocalArch.str_in_list_regex(str(file.name), exceptions)
            if file.is_dir():
                continue
            if not keep:
                file.unlink()

        return

    def clean_scratch_dir(self,
                          exceptions: Optional[List[str]] = []
                          ) -> None:
        """Clean scratch directory.

        Parameter
        ---------
        exceptions: List[str], optional
            list of exceptions, e.g. [ 'myfile', '*.inp' ]

        """
        return self.clean_dir(self.scratch_dir, exceptions)  # type: ignore

    def clean_work_dir(self, exceptions: Optional[List[str]] = []
                       ) -> None:
        """Clean working directory.

        Parameter
        ---------
        exceptions: List[str], optional
            list of exceptions, e.g. [ 'myfile', '*.inp' ]

        """
        return self.clean_dir(self.work_dir, exceptions)  # type: ignore

    @classmethod
    def str_in_list_regex(cls, instr: str, inlist: List[str]) -> bool:
        """Find str in list. list meight be either string or regex.

        Parameter
        ---------
        instr : str
            str to be looked for, uses '*' as bash-like wilcard
        inlist : List[str]
            list of strings that could potentially contain instr

        Returns
        -------
        bool
            if instr is in inlist

        """
        match = False
        import re
        for e in inlist:
            regex = e.replace('*', '.*')
            try:
                pattern = re.compile(regex)
                match = bool(pattern.fullmatch(instr))
            except Exception:
                match = (instr == e)
            finally:
                if match:
                    return True

        return False
