import subprocess
import warnings
from contextlib import contextmanager
from pathlib import Path
from typing import Generator, Union

from ..runner import Runner

stderr_exceptions = ['normal termination of xtb',
                     'Cannot open input file: gibberish',
                     'src/start/cp2k.F (unit = 6, file = ',
                     'DEEPMD INFO'
                     ]


class LocalRunner(Runner):
    """Run computation on local architecture."""

    def __init__(self, env):

        self.work_dir = env.work_dir
        self.force_recompute = env.force_recompute
        self.env = env.env
        self.print_level = env.print_level
        self.launcher = env.launcher
        self.debug = env.debug

    @contextmanager
    def run_ctx(self, input_dict) -> Generator[str, None, None]:
        """Run a computaion inside a context manager.

        Parameter
        ---------
        input_dict : dict
            input

        Returns
        -------
        Generator
            generates context

        """
        output = str(self.run(input_dict))
        try:
            yield output
        finally:
            pass

    def run(self, input_dict: dict) -> str:
        """Run a computation locally.

        Parameters
        ----------
        input_dict: dict
            input dictionary containing 'program' and iff applicable
            'args' and 'output_file'

        Returns
        -------
        str
            name of outputfile or captured standardoutput

        """
        program = input_dict.get('program', None)
        if program is None:
            raise KeyError('program name required')

        launcher_input: Union[str, list] = input_dict.get('launcher',
                                                          self.launcher.copy())
        launcher: list
        if isinstance(launcher_input, str):
            launcher = launcher_input.split()
        else:
            launcher = launcher_input
        launcher.append(program)

        args = input_dict.get('args', [])

        work_dir = input_dict.get('work_dir', self.work_dir)

        output_file = input_dict.get('output_file', None)
        stdout_redirect = input_dict.get('stdout_redirect', None)

        if output_file is not None:
            output_file = Path(output_file)
            if output_file.exists():
                w: str = f'output file {output_file} already exists '
                if self.force_recompute:
                    if self.print_level > 0:
                        warnings.warn(w + 'and will be overwritten')
                else:
                    if self.print_level > 0:
                        warnings.warn(w + 'and will*not* be overwritten')
                    if self.debug:
                        print('DEBUG: RUNNING NOTHING!')
                    return str(output_file)

        if self.debug:
            print('DEBUG: RUNNING ' + ' '.join([*launcher, *args]))

        result = subprocess.run([*launcher, *args],
                                capture_output=True,
                                text=True,
                                cwd=work_dir,
                                env=self.env,
                                shell=False)

        if result.returncode < 0:
            if self.print_level > 0:
                warnings.warn(f'{program} run failed, ' +
                              'returncode: {result.returncode}')
        if result.stderr:
            if all([x not in result.stderr for x in stderr_exceptions]):
                if self.print_level > 0:
                    warnings.warn(result.stderr)
            result.stdout += result.stderr

        if result.stdout:
            if stdout_redirect is not None:
                stdout_redirect = Path(stdout_redirect)
                with open(stdout_redirect, 'wt') as f:
                    f.write(result.stdout)
                return str(stdout_redirect)
            else:
                return result.stdout

        return str(output_file)
