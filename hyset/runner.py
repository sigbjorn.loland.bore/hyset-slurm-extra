from abc import ABC
from typing import Callable


class Runner(ABC):
    """Base class for runners."""

    run: Callable
    # run_async: Callable
