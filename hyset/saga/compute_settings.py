import pathlib
from datetime import timedelta
from typing import List, Union

import paramiko

from ..abc import ComputeSettings
from .runner import SagaRunner

PathLike = Union[str, pathlib.Path]


def get_ssh_username_from_default_config(ssh_host: str) -> str:
    """Get SSH username from default configuration locations using paramiko.

    Parameters
    ----------
    ssh_host : str
        SSH host.

    Returns
    -------
    str
        SSH username.

    """
    ssh_config = paramiko.SSHConfig()
    user_config_file = pathlib.Path.home() / '.ssh' / 'config'
    if user_config_file.is_file():
        with user_config_file.open() as f:
            ssh_config.parse(f)
    system_config_file = pathlib.Path('/etc/ssh/ssh_config')
    if system_config_file.is_file():
        with system_config_file.open() as f:
            ssh_config.parse(f)
    return ssh_config.lookup(ssh_host)['user']


def format_timedelta(t: timedelta) -> str:
    """Convert a timedelta object to SLURM string format {D}-{H}:{M}:{S}.

    Parameters
    ----------
    t : datetime.timedelta
        Input timedelta object.

    Returns
    -------
    str
        String containing a {days}-{hours}:{minutes}:{seconds} repesentation
        of the input timedelta object.

    """
    days, seconds = divmod(int(t.total_seconds()), 86400)
    hours, seconds = divmod(int(seconds), 3600)
    minutes, seconds = divmod(int(seconds), 60)
    return f'{days}-{hours:02d}:{minutes:02d}:{seconds:02d}'


class SagaArch(ComputeSettings):
    """Saga compute settings."""

    ssh_hostname = 'saga.sigma2.no'

    def __init__(
        self,
        work_dir: PathLike = None,
        scratch_dir: PathLike = None,
        ssh_username: str = None,
        slurm_account: str = 'nn4654k',
        modules: List[str] = [],
        job_name: str = None,
        job_time: Union[str, timedelta] = timedelta(minutes=10),
        memory_per_cpu: int = 2000,
        cpus_per_task: int = 1,
        ntasks: int = 1,
        qos_devel: bool = False,
    ):
        """Initialize Saga compute settings.

        Parameters
        ----------
        work_dir : PathLike, optional
            Local working directory, defaults to current working directory if
            `None`.
        scratch_dir : PathLike, optional
            Local scratch directory, defaults to current working directory if
            `None`.
        ssh_username : str
            Username for SSH connection.
        slurm_account : str, optional
            SLURM account, by default "nn4654k".
        modules : List[str], optional
            Modules to load.
        job_name : str, optional
            Job name, generated by default if not provided.
        job_time : Union[str, datetime.timedelta], optional
            Job time, either a string in SLURM format or a timedelta object
            which is converted to SLURM format.
        memory_per_cpu : int, optional
            Memory per CPU in MB, by default 2GB.
        cpus_per_task : int, optional
            CPUs per task, by default 1.
        ntasks : int, optional
            Number of tasks, by default 1.
        qos_devel : bool, optional
            Use development quality of serivce (QOS), by default False.

        """
        if work_dir is not None:
            self.work_dir = pathlib.Path(work_dir)
            if not self.work_dir.exists():
                self.work_dir.mkdir(parents=True)
        else:
            self.work_dir = pathlib.Path.cwd()

        if scratch_dir is not None:
            self.scratch_dir = pathlib.Path(scratch_dir)
            if not self.scratch_dir.exists():
                self.scratch_dir.mkdir(parents=True)
        else:
            self.scratch_dir = pathlib.Path.cwd()

        if ssh_username is None:
            ssh_username = get_ssh_username_from_default_config('saga')
        self.job_name = job_name
        self.ssh_username = ssh_username
        self.slurm_account = slurm_account
        self.modules = modules
        self.job_time = job_time
        if isinstance(job_time, timedelta):
            self.slurm_time = format_timedelta(job_time)
        else:
            self.slurm_time = job_time
        self.memory_per_cpu = memory_per_cpu
        self.cpus_per_task = cpus_per_task
        self.ntasks = ntasks
        self.qos_devel = qos_devel
        self.Runner = SagaRunner(self)

        self.mpi_procs = ntasks
        self.memory = self.memory_per_cpu

    @property
    def name(self) -> str:
        """Print environment type."""
        return 'saga'

    def find_bin_in_env(self, program: str) -> str:
        """Find binary in environment.

        Hijacks the find_bin_in_env function from the LocalArch compute
        settings in order to seamlessly work with interfaces expecting full
        paths to binaries. In reality, just feeds the program name back.

        Parameters
        ----------
        program : str
            Program name.

        Returns
        -------
        str
            The input `program` name.

        """
        return program
