import pathlib
import re
import time
from datetime import datetime, timedelta
from typing import TYPE_CHECKING, Any, Dict, List, Union

import fabric
import tqdm

from ..runner import Runner

if TYPE_CHECKING:
    from .compute_settings import SagaArch

from .slurm import SLURMJob, fetch_all_slurm_jobs, fetch_job_info


def _parse_slurm_time_string(slurm_time_str: str) -> timedelta:
    """Parse a SLURM time string.

    Parse a SLURM time string in the form output from

        squeue -o '%V'

    or

        squeue -o '%L'

    and turn it into a datetime.timedelta object.


    Parameters
    ----------
    slurm_time_str : str
        SLURM time string.

    Returns
    -------
    datetime.timedelta
        Time delta object representing the .

    """
    days: str = '0'
    hours: str = '0'
    minutes: str = '0'
    seconds: str = '0'
    if '-' in slurm_time_str:
        days, slurm_time_str = slurm_time_str.split('-')
    if ':' in slurm_time_str:
        split_str: List[str] = slurm_time_str.split(':')
        if len(split_str) == 3:
            hours = split_str[0]
            minutes = split_str[1]
            seconds = split_str[2]
        elif len(split_str) == 2:
            minutes = split_str[0]
            seconds = split_str[1]
        elif len(split_str) == 1:
            seconds = split_str[0]
    return timedelta(
        days=int(days),
        hours=int(hours),
        minutes=int(minutes),
        seconds=int(seconds)
    )


def _tqdm_update_to(
        progress_bar: tqdm.tqdm,
        job: SLURMJob,
        finished=False
) -> None:
    """Update a tqdm progress bar to a specific progress value.

    Parameters
    ----------
    progress_bar : tqdm.tqdm
        Progress bar to update.
    job : SLURMJob
        SLURM job info object.
    finished : bool, optional
        Whether the job has finished, by default `False`.

    """
    time_left_delta: timedelta = _parse_slurm_time_string(job.time_left)
    elapsed_time_delta: timedelta = _parse_slurm_time_string(job.elapsed_time)
    progress: int = round(
        (elapsed_time_delta / (elapsed_time_delta + time_left_delta)) * 100
    )
    progress_bar.n = progress
    progress_bar.last_print_n = progress
    if progress_bar.desc != f'{job.id:<10}{job.state:>12}':
        progress_bar.desc = f'{job.id:<10}{job.state:>12}'
    if finished:
        progress_bar.n = progress_bar.total
        progress_bar.last_print_n = progress_bar.total
    progress_bar.refresh()


class SagaRunner(Runner):
    """Runner for the Sigma2 Saga machine."""

    def __init__(self, compute_settings: 'SagaArch') -> None:
        """Initialize Saga runner.

        Parameters
        ----------
        compute_settings : SagaArch
            Saga compute settings object.

        """
        self.compute_settings = compute_settings

    def generate_job_name(self, program) -> str:
        """Generate SLURM job name.

        Parameters
        ----------
        program : str
            Program name.

        Returns
        -------
        str
            Job name generated from the current time and date.

        """
        return f"{program}_{datetime.now().strftime('%Y-%m-%d_%H:%M:%S')}"

    def run(self, input_dict: Dict[str, str]) -> str:
        """Run a computation on the Sigma2 Saga machine.

        Parameters
        ----------
        input_dict : Dict[str, str]
            Input dictionary containing 'program' and iff applicable
            'args' and 'output_file'.

        Returns
        -------
        str
            Name of outputfile or captured standardoutput.

        """
        args: Any = input_dict.get('args', [])

        program: Union[str, None] = input_dict.get('program', None)
        if program is None:
            raise KeyError('Program name required')
        job_name: Union[str, None] = self.compute_settings.job_name
        if job_name is None:
            job_name = self.generate_job_name(program)

        # Send input files to Saga
        input_files: List[pathlib.Path] = []
        for arg in args:
            if arg.endswith('.inp'):
                args.remove(arg)
                input_files.append(pathlib.Path(arg))
        input_files_remote: List[str] = self.send_files_to_remote(input_files)

        modules: List[str] = self.compute_settings.modules
        job_time: str = self.compute_settings.slurm_time
        memory_per_cpu: int = self.compute_settings.memory_per_cpu
        cpus_per_task: int = self.compute_settings.cpus_per_task
        ntasks: int = self.compute_settings.ntasks
        slurm_account: str = self.compute_settings.slurm_account

        # Generate and send job script to Saga
        self.job_script_file = self.generate_job_script_file(
            program=program,
            args=args,
            input_files=input_files_remote,
            job_name=job_name,
            job_time=job_time,
            memory_per_cpu=memory_per_cpu,
            cpus_per_task=cpus_per_task,
            ntasks=ntasks,
            modules=modules,
            slurm_account=slurm_account,
            qos_devel=self.compute_settings.qos_devel,
        )
        sent_job_script: List[str] = self.send_files_to_remote(
            [pathlib.Path(self.job_script_file)]
        )

        # Connect to Saga and start job
        connection: fabric.Connection = fabric.Connection(
            host=self.compute_settings.ssh_hostname,
            user=self.compute_settings.ssh_username,
        )
        connection.run(
            f'cd /cluster/work/users/{self.compute_settings.ssh_username}',
            hide=True,
        )
        connection.run(f'chmod +x {sent_job_script[0]}', hide=True)
        slurm_start_result = connection.run(
            f'sbatch {sent_job_script[0]}', hide=True
        )

        # Extract job ID and monitor job for completion
        re_string: str = r'(?<=Submitted batch job )\d+'  # noqa: W605
        job_id: int = re.findall(re_string, slurm_start_result.stdout)[0]
        job_finished = False
        time_out: float = 0.0

        total_job_time: int = int(
            _parse_slurm_time_string(job_time).total_seconds()
        )
        progress_bar = tqdm.tqdm(
            total=total_job_time,
            ncols=120,
            colour='#2b38ff',
            bar_format=f'{{l_bar}}{{bar}}| {{elapsed}} < {job_time}')
        _tqdm_update_to(
            progress_bar=progress_bar,
            job=SLURMJob(
                f'{job_id} _ _ SUBMITTING _ 0:00 {job_time}'
            )
        )

        while job_finished is not True:
            # Extend the time out between checks so we don't spam the server,
            # maxing out at one minute
            time.sleep(time_out)
            if time_out < 60:
                time_out += 0.5

            jobs_info: List[SLURMJob] = fetch_all_slurm_jobs(
                ssh_host=self.compute_settings.ssh_hostname,
                ssh_username=self.compute_settings.ssh_username,
                slurm_username=self.compute_settings.ssh_username,
            )
            job_in_queue: bool = False
            job_info: SLURMJob
            for job_info in jobs_info:
                if job_info.name == job_name or job_info.id == job_id:
                    job_in_queue = True
                    _tqdm_update_to(progress_bar=progress_bar, job=job_info)

            if job_in_queue is False:
                job_finished = True

                job_info = fetch_job_info(
                    job_id=job_id,
                    ssh_host=self.compute_settings.ssh_hostname,
                    ssh_username=self.compute_settings.ssh_username,
                    slurm_username=self.compute_settings.ssh_username,
                )
                _tqdm_update_to(progress_bar=progress_bar,
                                job=job_info,
                                finished=True)

        progress_bar.close()
        username: str = self.compute_settings.ssh_username
        result_file: pathlib.Path = self.fetch_files_from_remote(
            [f'/cluster/work/users/{username}/{job_id}/result.out']
        )[0]
        result_file.rename(input_dict['output_file'])
        return str(result_file)

    def send_files_to_remote(self, files: List[pathlib.Path]) -> List[str]:
        """Send files to remote machine with SCP using fabric.

        Parameters
        ----------
        files : List[pathlib.Path]
            List of files to send.

        Returns
        -------
        sent_files_path : List[str]
            List of remote paths to the sent files.

        """
        sent_files_path: List[str] = []
        ssh_username = self.compute_settings.ssh_username
        for file_ in files:
            result: fabric.transfer.Result = fabric.Connection(
                host=self.compute_settings.ssh_hostname,
                user=self.compute_settings.ssh_username,
            ).put(file_, remote=f'/cluster/work/users/{ssh_username}/')
            sent_files_path.append(result.remote)
        return sent_files_path

    def fetch_files_from_remote(self, files: List[str]) -> List[pathlib.Path]:
        """Fetch files from remote machine with SCP using fabric.

        Parameters
        ----------
        files : List[str]
            List of files to fetch.

        Returns
        -------
        fetched_files_path : List[pathlib.Path]
            List of local paths to fetched files.

        """
        fetched_files_path: List[pathlib.Path] = []
        for file_ in files:
            result: fabric.transfer.Result = fabric.Connection(
                host=self.compute_settings.ssh_hostname,
                user=self.compute_settings.ssh_username,
            ).get(file_)  # , local=f"{self.compute_settings.work_dir}")
            fetched_files_path.append(pathlib.Path(result.local))
        return fetched_files_path

    def generate_job_script_file(
            self,
            program: str,
            args: List[str],
            input_files: List[str],
            job_name: str,
            job_time: str,
            memory_per_cpu: int,
            cpus_per_task: int,
            ntasks: int,
            modules: List[str],
            slurm_account: str,
            qos_devel: bool,
    ) -> pathlib.Path:
        """Generate SLURM job script file for running the `program`.

        Uses the specified arguments and loads the specified modules.

        The SLURM script creates a work directory with the same name as the
        JobID under '/cluster/work/users/{USERNAME}/{JOB_ID}', and copies the
        input file(s) there. Then the program is run with the specified
        arguments.

        Parameters
        ----------
        program : str
            Name of the program to run on Saga. Assumed to be in $PATH after
            `modules` are loaded.
        args : List[str]
            List of arguments to pass to the program.
        input_files : List[str]
            List of input files to copy to the work directory on Saga.
        job_name : str
            Name of the generated SLURM job.
        job_time : str
            Time limit for the SLURM job, in the format D-HH:MM:SS.
        memory_per_cpu : int
            Memory per CPU core, in MB.
        cpus_per_task : int
            Number of CPU cores to use per task.
        ntasks : int
            Number of MPI tasks to run.
        modules : List[str]
            List of modules to load.
        slurm_account : str
            SLURM account to use.
        qos_devel : bool
            Whether to use the 'devel' quality of service (QOS).

        Returns
        -------
        pathlib.Path
            Path to the generated job script file.

        """
        with open('job_script.sh', 'w') as f:
            f.write(
                f'#!/bin/bash\n'
                f'#SBATCH --job-name={job_name}\n'
                f'#SBATCH --time={job_time}\n'
                f'#SBATCH --mem-per-cpu={memory_per_cpu}\n'
                f'#SBATCH --cpus-per-task={cpus_per_task}\n'
                f'#SBATCH --ntasks={ntasks}\n'
                f'#SBATCH --account={slurm_account}\n'
                f'#SBATCH --output={job_name}.out\n'
                f'#SBATCH --error={job_name}.err\n'
            )
            if qos_devel:
                f.write('#SBATCH --qos=devel\n')

            f.write(
                f'\nset -o errexit\n\n'
                f'# Load modules\n'
                f"{' '.join([f'module load {module}' for module in modules])}\n"  # noqa: E501
                f'set -x\n\n'
            )

            username: str = self.compute_settings.ssh_username
            f.write(
                f'# Create work directory\n'
                f'export SCRATCH="/cluster/work/users/{username}/${{SLURM_JOB_ID}}"\n'  # noqa: E501
                f'mkdir -p ${{SCRATCH}}\n\n'
                f'# Copy input file(s) to work directory\n'
                f"{' '.join([f'cp {input_file} ${{SCRATCH}}' for input_file in input_files])}\n\n"  # noqa: E501
            )

            f.write(
                f'# Run program\n'
                f'cd ${{SCRATCH}}\n'
                f"{program} {' '.join(args)} {' '.join(input_files)} > result.out\n"  # noqa: E501
            )
        return pathlib.Path('job_script.sh')
