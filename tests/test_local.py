import hyset


def test_create_cs():
    """Test creation of ComputeSettings object."""
    myenv = hyset.create_compute_settings()
    assert myenv.name == 'local'


def test_local_cs():
    """Test methods in LocalArch."""
    myenv = hyset.create_compute_settings()
    hyset.LocalArch.create_path('mytestdir', create=True)
    out = myenv.Runner.run({'program': 'ls', 'args': ['-l']})
    assert 'mytestdir' in out
    hyset.LocalArch.delete_path('mytestdir')
    out = myenv.Runner.run({'program': 'ls', 'args': ['-l']})
    assert 'mytestdir' not in out
